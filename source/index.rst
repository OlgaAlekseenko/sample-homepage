.. Sample Sample Homepage index file, created by `ablog start` on Wed Nov 22 16:33:37 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Добро пожаловать в блог Автора
==============================

Привет, мир! Подробнее обо мне читайте в разделе :ref:`about`.

Hello!!!

Недавние посты в моем блоге:

.. postlist:: 5
   :excerpts:


.. `toctree` directive, below, contains list of non-post `.rst` files.
   This is how they appear in Navigation sidebar. Note that directive
   also contains `:hidden:` option so that it is not included inside the page.

   Posts are excluded from this directive so that they aren't double listed
   in the sidebar both under Navigation and Recent Posts.

..  Директива `toctree` собирает документы в оглавление, которое показывается слева в панели навигации.
    Атрибут `:hidden:` нужен, чтобы оглавление не дублировалось на странице.

    Не включайте сюда посты — они и так появятся в разделе «Недавние Записи».

.. toctree::
   :hidden:

   about.rst

